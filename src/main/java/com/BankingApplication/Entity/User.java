package com.BankingApplication.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="user")
public class User {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    private UUID accountID;
    @Column(name = "username")
    private String userName;
    @Column(name = "email")
    private String email;
    @Column(name = "age")
    private Integer userAge;


    public User() {

    }

    public User(UUID accountID, String userName, String email, Integer userAge) {
        this.accountID = accountID;
        this.userName = userName;
        this.email = email;
        this.userAge = userAge;
    }

    public UUID getAccountID() {
        return accountID;
    }

    public void setAccountID(UUID accountID) {
        this.accountID = accountID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserAge() {
        return userAge;
    }

    public void setUserAge(Integer userAge) {
        this.userAge = userAge;
    }
}
