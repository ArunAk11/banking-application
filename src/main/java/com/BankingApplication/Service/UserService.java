package com.BankingApplication.Service;

import com.BankingApplication.Convertor.UserConvertor;
import com.BankingApplication.CustomExceptionHandler.EmptyDbException;
import com.BankingApplication.DTO.UserDto;
import com.BankingApplication.DTO.UserDtoResponse;
import com.BankingApplication.Entity.User;
import com.BankingApplication.Repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    UserConvertor userConvertor = new UserConvertor();
    Logger logger = LoggerFactory.getLogger(UserService.class);

    public Page<UserDtoResponse> getAllUsers(int pageNo, int noOfContents, String direction, String column) {
        Page<UserDtoResponse> userList = userRepository.findAll(PageRequest.of(pageNo, noOfContents, Sort.Direction.fromString(direction), column)).map(UserConvertor::userToResponseDto);
        if(userList.isEmpty())
        {
            logger.error("No records found in database.");
            throw new EmptyDbException("No records found in database.");
        }
        return userList;
    }


    public UserDto addUser(UserDto userDto) {
        User user = userConvertor.dtoToUser(userDto);
        userRepository.save(user);
        return UserConvertor.userToDto(user);
    }


    public UserDto updateUser(UserDto userDto, UUID id) {
        Optional<User> userById = userRepository.findById(id);
        User user;
        if (userById.isPresent()) {
            user = userRepository.save(UserConvertor.dtoToUser(userDto));
            return UserConvertor.userToDto(user);
        }
        throw new NoSuchElementException("No user account with given id :" + id + " id present in database");
    }
}
