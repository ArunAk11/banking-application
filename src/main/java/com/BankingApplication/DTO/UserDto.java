package com.BankingApplication.DTO;

import java.util.UUID;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserDto {

    @NotBlank(message = "Account ID cannot be empty.")
    @Size(min = 11, message = "Account ID must have at least 11 characters")
    private UUID accountID;
    @NotBlank(message = "Username cannot be empty.")
    @Size(min = 5, message = "Username must have at least 5 characters")
    private String userName;
    @NotBlank(message = "Email ID is mandatory")
    private String email;
    @NotBlank(message = "Age cannot be empty.")
    @Size(min = 1, message = "Username must have at least 1 characters")
    private Integer userAge;



    public UserDto() {
    }

    public UserDto(UUID accountID, String userName, String email, Integer userAge) {
        this.accountID = accountID;
        this.userName = userName;
        this.email = email;
        this.userAge = userAge;
    }

    public UUID getAccountID() {
        return accountID;
    }

    public void setAccountID(UUID accountID) {
        this.accountID = accountID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserAge() {
        return userAge;
    }

    public void setUserAge(Integer userAge) {
        this.userAge = userAge;
    }
}

