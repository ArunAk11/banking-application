package com.BankingApplication.Controller;

import com.BankingApplication.DTO.UserDto;
import com.BankingApplication.DTO.UserDtoResponse;
import com.BankingApplication.Service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;


@RestController
@RequestMapping("/v1/user")
public class UserController {


    @Autowired
    private UserService userService;

    Logger logger = LoggerFactory.getLogger(UserController.class);
    @GetMapping("/users/{pageNo}/{noOfContents}/{column}/{direction}")
    public ResponseEntity<Page<UserDtoResponse>> getAllUsers(@PathVariable int pageNo, @PathVariable int noOfContents, @PathVariable String direction, @PathVariable String column) {

        Page<UserDtoResponse> userlist = userService.getAllUsers(pageNo, noOfContents, direction, column);
        return new ResponseEntity<Page<UserDtoResponse>>(userlist, HttpStatus.OK);
    }

    @PostMapping("/user")
    public ResponseEntity<UserDto> addUser(@Valid @RequestBody UserDto userDto) {
        UserDto dto = userService.addUser(userDto);
        return new ResponseEntity<UserDto>(dto, HttpStatus.CREATED);
    }

    @PutMapping("/user/{accountID}")
         public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto, @PathVariable UUID accountID) {
           UserDto dto = userService.updateUser(userDto, accountID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }




}

