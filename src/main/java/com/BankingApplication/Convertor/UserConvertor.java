package com.BankingApplication.Convertor;

import com.BankingApplication.DTO.UserDto;
import com.BankingApplication.DTO.UserDtoResponse;
import com.BankingApplication.Entity.User;

public class UserConvertor {

    public static User dtoToUser(UserDto userDto){
        User user = new User();
        user.setAccountID(user.getAccountID());
        user.setUserName(user.getUserName());
        user.setEmail(user.getEmail());
        user.setUserAge(user.getUserAge());
        return user;
    }

    public static UserDto userToDto(User user){
        UserDto userDto = new UserDto();
        userDto.setAccountID(user.getAccountID());
        userDto.setUserName(user.getUserName());
        userDto.setEmail(user.getEmail());
        userDto.setUserAge(user.getUserAge());
        return userDto;

    }
    public static UserDtoResponse userToResponseDto(User user) {
        UserDtoResponse userRequestDto = new UserDtoResponse();
        userRequestDto.setUserName(user.getUserName());
        return userRequestDto;
    }
}
